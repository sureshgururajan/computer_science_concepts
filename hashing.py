# Demonstrating
# Hash table which handles:
# 1. No collisions (overwrite data for same key)
# 2. a. Linear probing to handle collision
#    b. Chaining

class BasicHashTable:
    def __init__(self, num_buckets=10):
        self.__hash_table = [0 for i in range(num_buckets)]
        self.__num_buckets = num_buckets


    def insert(self, key, value):
        bucket = self.fn(key) % self.__num_buckets
        self.__hash_table[bucket] = value


    def fn(self, key):
        return hash(key)
    

    def get(self, key):
        bucket = self.fn(key) % self.__num_buckets
        return self.__hash_table[bucket]


    def __str__(self):
        return ",".join([str(i) for i in self.__hash_table])


def test_basic():
    ht = BasicHashTable()

    for i in range(20):
        ht.insert("Key" + str(i), "Value" + str(i))
        print(ht)


if __name__ == "__main__":
    test_basic()
